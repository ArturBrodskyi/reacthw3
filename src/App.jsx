import { useState, useEffect } from "react";
import { useImmer } from "use-immer";
import axios from "axios";
import ItemList from "./components/itemList";
import "./App.scss";
import Counter from "./components/Cart&FavCounter";
import Modal from "./components/Modal";
import Router from "./Router.jsx";
import Header from "./components/Header";

function App() {
  const URL = "./arr.JSON";
  const [items, setItems] = useImmer([]);
  const [cart, setCart] = useImmer([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  useEffect(function () {
    async function fetchItems() {
      const { data } = await axios.get(URL);
      setItems(data);
    }
    fetchItems();
  }, []);

  const openModal = (item) => {
    setSelectedItem(item);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const openDeleteModal = (item) => {
    setSelectedItem(item);
    setIsDeleteModalOpen(true)
  }

  const closeDeleteModal = () => {
    setIsDeleteModalOpen(false)
  }

  const addToCart = (item) => {
    setCart((draft) => {
      const cartItem = draft.find(({ articul }) => articul === item.articul);
      if (!cartItem) {
        draft.push({ ...item, count: 1 });
      } else {
        cartItem.count++;
      }

      localStorage.setItem("cart", JSON.stringify(draft));
    });

    closeModal();
  };
  const removeFromCart = (item) => {
    setCart((draft) => {
      const index = draft.findIndex(
        (cartItem) => cartItem.articul === item.articul
      );
      if (index !== -1) {
        draft.splice(index, 1);
      }
      localStorage.setItem("cart", JSON.stringify(draft));
    });

    closeDeleteModal()
  };
  useEffect(() => {
    const itemLs = localStorage.getItem("cart");
    if (itemLs) {
      setCart(JSON.parse(itemLs));
    }
  }, []);
  useEffect(() => {
    const itemFav = localStorage.getItem("fav");
    if (itemFav) {
      setItems(JSON.parse(itemFav));
    }
  }, []);

  const CartCounter = () => {
    const cartLenght = cart.length;
    return cartLenght;
  };

  const FavCounter = () => {
    const favourite = items.filter((item) => item.isFavourite === true);
    const favNumber = favourite.length;
    return { favNumber, favourite };
  };

  const FavouriteFn = (articul) => {
    setItems((draft) => {
      const itemIndex = draft.findIndex((item) => articul === item.articul);
      if (itemIndex !== -1) {
        draft[itemIndex].isFavourite = !draft[itemIndex].isFavourite;
        localStorage.setItem("fav", JSON.stringify(draft));
      }
    });
  };
  const cartNumber = CartCounter();
  const { favNumber, favourite } = FavCounter();
  return (
    <>
      <Header></Header>
      <Router
        items={items}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
        addToCart={addToCart}
        cartNumber={cartNumber}
        favNumber={favNumber}
        cart={cart}
        removeFromCart={removeFromCart}
        openDeleteModal={openDeleteModal}
        favourite={favourite}
        
      ></Router>

      {selectedItem && (
        <Modal
          headerCont="Add to cart?"
          bodyCont={`Add ${selectedItem.name} to cart?`}
          firstButtonText="Add to cart"
          addToCart={() => addToCart(selectedItem)}
          className="fav-modal"
          isModalOpen={isModalOpen}
          onCloseClick={closeModal}
        ></Modal>
      )}
      {selectedItem && (
      <Modal
        headerCont="Remove from cart?"
        bodyCont={`Remove ${selectedItem.name} from cart?`}
        firstButtonText="Ok"
        removeFromCart={() => removeFromCart(selectedItem)}
        className="fav-modal"
        isDeleteModalOpen={isDeleteModalOpen}
        closeDeleteModal={closeDeleteModal}
      ></Modal>
    )}
    </>
  );
}
export default App;
