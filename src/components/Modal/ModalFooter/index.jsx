const ModalFooter = ({firstText, firstClick}) =>{
    return(
        <div className="modal-footer">
                <button className={`${firstText}-btn footer-btn`} onClick={firstClick}>{firstText}</button>
        </div>
    )
}

export default ModalFooter;