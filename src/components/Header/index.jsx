import { Link } from "react-router-dom";
import styles from './Header.module.scss'

const Header = () => {
  return (
    <header>
      <ul className={styles.headerList}>
        <li>
          <Link to={"/"}>Home</Link>
        </li>
        <li>
          <Link to={"/cart"}>Cart</Link>
        </li>
        <li>
          <Link to={"/favourite"}>Favourite</Link>
        </li>
      </ul>
    </header>
  );
};

export default Header;
