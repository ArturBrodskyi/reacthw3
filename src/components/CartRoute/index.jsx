import ItemCartList from "../ItemCartList";

const CartRoute = ({cart, removeFromCart, openDeleteModal}) => {
    return(
<ItemCartList items={cart} removeFromCart={removeFromCart} openDeleteModal={openDeleteModal}></ItemCartList>
    )
}

export default CartRoute;