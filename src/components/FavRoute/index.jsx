import ItemList from "../itemList";
import Counter from "../Cart&FavCounter";
const FavRoute = ({favourite, FavouriteFn, openModal}) => {
  return (
    <>
      <ItemList
        items={favourite}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
      ></ItemList>
    </>
  );
};

export default FavRoute;
